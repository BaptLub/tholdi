<?php
session_start();

include_once 'gestionnaireTholdi.php';

/*
 * Test pour vérifier si la personne vient de cliquer sur le formulaire permettant la déconnexion
 */
if(isset($_REQUEST["logout"])){
    session_unset();
}

/*
 * Si le formulaire d'&uthentification est validé et contient un login et un mot de passe
 */
if(isset($_REQUEST["login"]) && ($_REQUEST["mdp"])){
    //On vérifie l'existance d'un compte dans la BD via la requete vérifiation
    $resultat =  verification($_REQUEST["login"], $_REQUEST["mdp"]);
    //Si le compte existe
    if($resultat == true){
        //On affecte des données saisies dans le forumaire dans les variables de sessions
        $_SESSION["login"] = $_REQUEST["login"];
        $_SESSION["mdp"] = $_REQUEST["mdp"];
    }
}


/*
 * SI L'utilisateur ne se trouve pas dans la page "index"
 */
if($_SERVER['PHP_SELF'] != "/projects/index.php"){
    //on test afin de savoir si il existe une variable de session pour l'utilisateur courant
    if(!(isset($_SESSION["login"]) && ($_SESSION["login"]))){
        // si il est pas authentifié on le redirige vers index
        header("Location: index.php");
    }
}
?>