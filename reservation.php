<?php
session_start();
include_once 'gestionnaireTholdi.php'
?>

<html>
    <head>
        <link href="cssindex.css" rel="stylesheet" type="text/css"/>
        <title>THOLDI-Reservation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>


    <body>


        <ul>
            <li><a href="index.php">Accueil</a></li>
            <li><a href="services.php">Nos services</a></li>
            <li><a href="reservation.php">Reservation</a></li>
            <li><a href="Panier.php">Panier</a></li>
            <ul style="float:right;list-style-type:none;">
                <li><a class="active" href="info.php">A propos</a></li>
                <li><a href="inscription.php">Inscription</a></li>
                <li><a href="index.php">Connexion</a></li>
            </ul>
        </ul>

        <!-----------------------------------------date--------------->
        <a href="index.php"><img src="images/logo.jpg" width="200" height="60" alt=""/></a>

        <div style="padding:5px; width:400px; margin-top:auto; margin-left: auto; margin-right: auto;border:3px solid #408183; background-color:#b3d8d2; -moz-border-radius:20px; -khtml-border-radius:20px; -webkit-border-radius:20px; border-radius:20px;">
            <strong>Date</strong>: 

            <br> 

            <form method="post" action="traitementReservation.php"><div>

                    <p>
                        <label for="dateDebutReservation">Du :</label>
                        <input type="date" name="dateDebutReservation" id="pseudo" />

                        <br /><br/>
                        <label for="dateFinReservation">Au :</label>
                        <input type="date" name="dateFinReservation" id="pass" />

                    </p>

                </div>


                <br>
                <br>

                <strong>Information</strong>: 
                <br> <br>

                <!--  recupere toutes les villes dans la BDD grâce à la fonction "getVille" et les affiches-->
                <div>
                    <?php
                    $collectionVille = getVille();
                    ?>


                    <p>
                        <label for="villeDepart">Ville de départ :</label>                               
                        <SELECT name="villeDepart" size="1">
                            <?php $_SESSION['collectionVille'] = $collectionVille; ?> 
                            <?php
                            foreach ($collectionVille as $ville) :
                                ?>
                                <option value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?> </option>
                                <?php
                            endforeach;
                            ?>
                        </SELECT>
                    </p>




                    <label for="villeArrivee">Ville d'arrivé :</label>                               
                    <SELECT name="villeArrivee" size="1">
                        <?php
                        foreach ($collectionVille as $ville) :
                            ?>
                            <option value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?> </option>
                            <?php
                        endforeach;
                        ?>
                    </SELECT>



                </div>


                <br>
                <br>



                <?php
                $collectionTypeContainer = getTypeContainer();
                ?>

                <div>

                    <strong>Caracteristique</strong>:
                    <br><br/>

                    <label for="typeContainer">Type de container :</label>                               
                    <SELECT name="typeContainer" size="1">
                        <?php
                        foreach ($collectionTypeContainer as $container) :
                            ?>
                            <option value="<?php echo $container["typeContainer"]; ?>"><?php echo $container["libelleTypeContainer"]; ?> </option>
                            <?php
                        endforeach;
                        ?>
                    </SELECT>





                    <br />
                    <p>Quantité : 


                        <input for="quantite" name="quantite" type="number" value="1" min="1" max="25">
                    </p>


                    <label for="volumeEstime">Taille (en M³):</label>               
                    <SELECT name="volumeEstime" size="1">
                        <OPTION>20
                        <OPTION>40
                        <OPTION>40' x 8' x 9,6"
                        <OPTION>45' x 8' x 9,6"
                    </SELECT>
                    <input type="submit" class="button4" name="lien2" value="Valider"></div>
            </form>




        </div>

    </body>
</html>